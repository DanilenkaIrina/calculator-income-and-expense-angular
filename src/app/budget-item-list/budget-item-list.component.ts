import { Component, Input, OnInit, Output } from '@angular/core';
import { BudgetItem } from '../shared/models/budget-item.model';

@Component({
  selector: 'app-budget-item-list',
  templateUrl: './budget-item-list.component.html',
  styleUrls: ['./budget-item-list.component.scss']
})
export class BudgetItemListComponent implements OnInit {

  @Input() budgetsItem: BudgetItem[];

  constructor() { }

  ngOnInit(): void {
  }

  deleteItem(item: BudgetItem) {
    const index = this.budgetsItem.indexOf(item);
    this.budgetsItem.splice(index, 1);
  }
}
